import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class EnvironmentControllerTest {
    private HVAC hvacspy;
    private EnvironmentController ec;

    @Before
    public void setUp() throws Exception {
        hvacspy = new HVAC();
        ec = new EnvironmentController(hvacspy);
    }

    @Test
    public void shouldHaveDefaultValuesOnCreation() throws Exception {
        assertEquals(false, hvacspy.heat());
        assertEquals(false, hvacspy.cool());
        assertEquals(false, hvacspy.fan());
    }

    @Test
    public void shouldTurnHeatOnWhenTempBelow65() throws Exception {
        hvacspy.temp = 64;
        ec.tic();

        assertEquals(true, hvacspy.heat());
        assertEquals(false, hvacspy.cool());
        assertEquals(true, hvacspy.fan());
    }

    @Test
    public void shouldTurnACOnWhenTempAbove75() throws Exception {
        hvacspy.temp = 76;
        ec.tic();

        assertEquals(false, hvacspy.heat());
        assertEquals(true, hvacspy.cool());
        assertEquals(true, hvacspy.fan());
    }

    @Test
    public void shouldTurnHeatOffIfWarmEnough() throws Exception {
        turnHeatOn();
        hvacspy.temp = 70;

        ec.tic();

        assertEquals(false, hvacspy.heat());
    }

    @Test
    public void shouldTurnCoolerAndFanOffIfCoolEnough() throws Exception {
        turnCoolerOn();
        hvacspy.temp = 70;

        ec.tic();

        assertEquals(false, hvacspy.cool());
        assertEquals(false, hvacspy.fan());
    }

    @Test
    public void shouldKeepFanRunningFor5MinAfterHeatOff() throws Exception {
        turnHeatOn();
        hvacspy.temp = 70;

        assertFanOnFor5Min();

        ec.tic();
        assertEquals(false, hvacspy.fan());
    }

    @Test
    public void shouldNotStartCoolerFor3MinAfterTurnedOff() throws Exception {
        turnCoolerOn();
        turnCoolerOff();

        hvacspy.temp = 85;
        assertCoolerOffAtEndOf2ndMin();

        ec.tic();

        assertEquals(true, hvacspy.cool());
    }

    @Test
    public void shouldNotTurnFanOffIfHeatIsOn() throws Exception {
        turnHeatOn();
        turnHeatOff();

        hvacspy.temp = 60;
        wait10Tics();

        assertEquals(true, hvacspy.fan());
    }

    @Test
    public void shouldNotTurnFanOffIfCoolerIsOn() throws Exception {
        turnHeatOn();
        turnHeatOff();

        hvacspy.temp = 85;
        wait10Tics();

        assertEquals(true, hvacspy.fan());
    }

    private void turnHeatOff() {
        hvacspy.temp = 70;
        ec.tic();
    }

    private void turnHeatOn() {
        hvacspy.heat = true;
        hvacspy.cool = false;
        hvacspy.fan = true;
    }

    private void turnCoolerOn() {
        hvacspy.heat = false;
        hvacspy.cool = true;
        hvacspy.fan = true;
    }

    private void assertFanOnFor5Min() {
        for(int i = 0; i < 5; i++) {
            ec.tic();
            assertEquals(true, hvacspy.fan());
        }
    }

    private void assertCoolerOffAtEndOf2ndMin() {
        for(int i = 0; i < 2; i++) {
            ec.tic();
            assertEquals(false, hvacspy.cool());
        }
    }

    private void turnCoolerOff() {
        hvacspy.temp = 70;
        ec.tic();
    }

    private void wait10Tics() {
        for(int i = 0; i < 10; i++){
            ec.tic();
        }
    }
}
