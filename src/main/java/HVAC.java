
public class HVAC implements HVACInterface{

    public boolean heat;
    public boolean cool;
    public boolean fan;
    public int temp;

    @Override
    public boolean heat() {
        return heat;
    }

    @Override
    public boolean cool() {
        return cool;
    }

    @Override
    public boolean fan() {
        return fan;
    }

    @Override
    public int getTemp() {
        return temp;
    }
}
