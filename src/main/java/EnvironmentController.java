
public class EnvironmentController {

    private final HVAC hvac;
    private int fanCounter = 0;
    private int coolCounter = 0;

    public EnvironmentController(HVAC hvac) {
        this.hvac = hvac;
        setDefaults();
    }

    public void tic() {
        ticCounters();
        changeClimateControl();
    }

    // my private shit

    private void setDefaults() {
        hvac.heat = false;
        hvac.cool = false;
        hvac.fan = false;
    }

    private void ticCounters() {
        if (fanCounter > -1)
            --fanCounter;
        if (coolCounter > -1)
            --coolCounter;
    }

    private void changeClimateControl() {
        if (itsCold())
            turnHeatOn();
        if (itsHot() && coolerHasntBeenOnForAWhile())
            turnCoolerOn();
        if (heatIsOn() && itsWarmEnough())
            turnHeatOff();
        if (coolerIsOn() && itsColdEnough())
            turnCoolerOff();
        if (itsBeen5MinSinceHeaterTurnedOff())
            turnFanOff();
    }

    // nested private shit

    private boolean itsCold() {
        return hvac.getTemp() < 65;
    }

    private void turnHeatOn() {
        hvac.fan = true;
        hvac.heat = true;
        fanCounter = -1;
    }

    private boolean itsHot() {
        return hvac.getTemp() > 75;
    }

    private boolean coolerHasntBeenOnForAWhile() {
        return coolCounter <= 0;
    }

    private void turnCoolerOn() {
        hvac.fan = true;
        hvac.cool = true;
        fanCounter = -1;
    }

    private boolean heatIsOn() {
        return hvac.heat() == true;
    }

    private boolean itsWarmEnough() {
        return hvac.getTemp() >= 70;
    }

    private void turnHeatOff() {
        hvac.heat = false;
        fanCounter = 5;
    }

    private boolean coolerIsOn() {
        return hvac.cool() == true;
    }

    private boolean itsColdEnough() {
        return hvac.getTemp() <= 70;
    }

    private void turnCoolerOff() {
        hvac.cool = false;
        hvac.fan = false;
        coolCounter = 3;
    }

    private boolean itsBeen5MinSinceHeaterTurnedOff() {
        return fanCounter == 0;
    }

    private void turnFanOff() {
        hvac.fan = false;
    }
}
