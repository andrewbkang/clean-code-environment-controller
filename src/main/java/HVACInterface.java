
public interface HVACInterface {
    public boolean heat();
    public boolean cool();
    public boolean fan();
    public int getTemp();
}
